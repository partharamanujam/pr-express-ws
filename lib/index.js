"use strict";

var http = require("http");

var HTTP_UPGRADE_REQUIRED = 426;

function handleWebsocketUpgrade(app, wss) {
    return function handleHttpUpgrade(req, socket, upgradeHead) {
        var head, res;

        // create esponse and assign socket
        res = new http.ServerResponse(req);
        res.assignSocket(socket);
        // make a copy of upgradeHead
        head = new Buffer(upgradeHead.length);
        upgradeHead.copy(head);
        // clean-up socket on finish
        res.on("finish", function finishResponse() {
            res.socket.destroy();
        });
        // websocket handler
        res.upgradeWebsocket = function upgradeWebsocket(callback) {
            wss.handleUpgrade(req, socket, head,
                function handleWsUpgrade(client) {
                    wss.emit("connection" + req.url, client);
                    wss.emit("connection", client);
                    if (callback) {
                        callback(client);
                    }
                }
            );
        };
        // route express
        app(req, res);
    };
}

function checkWebsocketUpgrade(req, res, next) {
    if (res.upgradeWebsocket === undefined) {
        res.status(HTTP_UPGRADE_REQUIRED).send("Upgrade Required");
    } else {
        next();
    }
}

exports.handleWebsocketUpgrade = handleWebsocketUpgrade;
exports.checkWebsocketUpgrade = checkWebsocketUpgrade;