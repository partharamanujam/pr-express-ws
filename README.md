# pr-express-ws

Simple [ExpressJS](https://www.npmjs.com/package/express) based routing for [Websockets](https://www.npmjs.com/package/ws)

## Philosophy

Easy to use express-like routing when using websockets.

## Installation

```bash
$ npm install pr-express-ws
$ npm test
```

## APIs

The module exports two functions:
* checkWebsocketUpgrade
* handleWebsocketUpgrade

```js
var expressWs = require('pr-express-ws'),
    checkWebsocketUpgrade = expressWs.checkWebsocketUpgrade,
    handleWebsocketUpgrade = expressWs.handleWebsocketUpgrade;
```

## Usage

Refer to test [server.js](https://gitlab.com/partharamanujam/pr-express-ws/blob/master/test/server.js) implementation for usage details.

## Test

```bash
$ npm install # inside pr-express-ws
$ npm test
```
