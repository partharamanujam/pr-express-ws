"use strict";

var http = require("http"),
    express = require("express"),
    ws = require("ws"),
    expressWs = require("../index");


var app, wss, server;

// setup express
app = express();
app.get("/hello", function getHello(req, res) {
    res.end("hello");
});
app.get("/ws", expressWs.checkWebsocketUpgrade,
    function createWs(req, res) {
        res.upgradeWebsocket(function onWsUpgrade(websock) {
            if (!websock) {
                return;
            }
            websock.on("message", function onWsMessage(data) { // loopback
                websock.send(data);
            });
        });
    }
);

// setup ws-server
wss = new ws.Server({
    "noServer": true
});

// setup http-server
server = http.createServer(app);
server.on("upgrade", expressWs.handleWebsocketUpgrade(app, wss));

module.exports = server;