"use strict";

var assert = require("assert"),
    request = require("supertest"),
    WebSocket = require("ws");

/* globals describe, beforeEach, afterEach, it */
describe("testing express-ws", function testExpressWs() {
    var server;

    beforeEach(function before() {
        delete require.cache[require.resolve("./server")];
        server = require("./server"); // eslint-disable-line global-require
        server.listen(9090); // eslint-disable-line no-magic-numbers
    });
    afterEach(function after(done) {
        server.close(done);
    });
    it("GET /hello", function getHello(done) {
        request(server)
            .get("/hello")
            .expect(200, done); // eslint-disable-line no-magic-numbers
    });
    it("GET /ws", function getWS(done) {
        request(server)
            .get("/ws")
            .expect(426, done); // eslint-disable-line no-magic-numbers
    });
    it("GET with Upgrade /ws", function testWs(done) {
        var msg = "hello world",
            ws = new WebSocket("ws://localhost:9090/ws");

        ws.on("message", function onMessage(data) {
            assert.equal(data, msg);
            ws.close();
            done();
        });
        ws.on("open", function onOpen() {
            ws.send(msg);
        });
    });
});